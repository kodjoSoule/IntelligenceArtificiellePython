# IntelligenceArtificiellePython
Ensemble de petits codes python relatif à l'IA

des notebooks à ouvrir avec [colab](https://colab.research.google.com) de préférence ou avec les notebooks de la suite [anaconda](https://www.anaconda.com/distribution/) : 

## Réseaux de neurones
- **très petits exemples pour comprendre les réseaux simples**
  - [TestET.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/TestET.ipynb)	: petit notebook python montrant la définition d'un réseau de neurones avec tensorflow pour l'apprentissage du ET logique
  - [TestET-TensorBoard.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/TestET-TensorBoard.ipynb)	: petit notebook python montrant la définition d'un réseau de neurones avec tensorflow pour l'apprentissage du ET logique. Et l'utilisation de l'outil TensorBoard pour étudier l'apprentissage du réseau défini.
  - [TestOUX.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/TestOUX.ipynb)	: petit notebook python montrant la définition d'un réseau de neurones avec tensorflow pour l'apprentissage du OU Exclusif logique
  - [DetectionAlertes.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/DetectionAlertes.ipynb)   :  petit exercice en notebook python de la détection très simple de messages douteux par réseaux de neurones
  - [SolutionDetectionAlertes.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/SolutionDetectionAlertes.ipynb)   : solution au petit exercice en notebook python de la détection très simple de messages douteux par réseaux de neurones
  - [SolutionDetectionAlertesTensorBoard.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/SolutionDetectionAlertesTensorBoard.ipynb)   : solution au petit exercice en notebook python de la détection très simple de messages douteux par réseaux de neurones. Et l'utilisation de l'outil TensorBoard pour étudier l'apprentissage du réseau défini.
  - [ClasserMail.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/ClasserMail.ipynb)   :  petit exercice en notebook python de la classification très simple de mails par réseaux de neurones
  - [SolutionClasserMail.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/SolutionClasserMail.ipynb)   : solution au petit exercice en notebook python de la classification très simple de mails par réseaux de neurones
  - [PredireReussite.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/PredireReussite.ipynb)   : Exercice de prédiction de réussite scolaire selon le contexte personnel et social
  - [PredireReussiteSolution.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/PredireReussiteSolution.ipynb)   : Solution à l'exercice de prédiction de réussite scolaire selon le contexte personnel et social. Solution où le réseau donne 1 seule valeur de sortie, à multiplier pour estimer la note à l'examen
  - [PredireReussiteSolutionBis.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/PredireReussiteSolutionBis.ipynb)   : Un autre solution à l'exercice de prédiction de réussite scolaire selon le contexte personnel et social. Solution où le réseau donne 8 valeurs de sortie, estimant les probabilité d'obtenir des notes entre Echec (neurone 0) et AA (neurone 7)
  
  
- **quasi deep-learning**
  - [SolutionTPDetectionDeSentiments.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/SolutionTPDetectionDeSentiments.ipynb)	: notebook en python montrant l'apprentissage par réseaux de neurones pour la classification de textes
  - [TPClassementDeTheses.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/TPClassementDeTheses.ipynb)	: notebook en python sur le TP de classement automatique de thèses scientifiques


## Logique floue 
- [choixSejourLogiqueFloueSolution.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/choixSejourLogiqueFloueSolution.ipynb)	: notebook python montrant l'utilisation de la logique floue pour prendre une décision sur la longueur de congés
- [logique_floue.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/logique_floue.ipynb) : notebook python montrant l'utilisation de la logique floue pour prendre une décision sur l'urgence de freiner en fonction de la position et de la taille d'un piéton sur le trottoir
- [choixClimLogiqueFloue.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/choixClimLogiqueFloue.ipynb) : TP en notebook python sur l'aide à la décision pour la régulation d'un climatiseur.

## Machine Learning via GYM
- [TPMLIntroGym.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/TPMLIntroGym.ipynb) : présentation de l'environnement de test [Gym](https://gym.openai.com) (Open AI)
- [MLGymGALunar.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/MLGymGALunar.ipynb) : sujet sur la réalisation d'un algo génétique pour l'environnement Lunar Lander.
- [MLGymGAMoutainCar.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/MLGymGAMoutainCar.ipynb) : sujet sur la réalisation d'un algo génétique pour l'environnement MoutainCar. 
- [TPMLGymQLearning.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/TPMLGymQLearning.ipynb) : sujet sur la réalisation d'un algo de Q-Learning pour l'évolution dans un labyrinthe gelé de Gym.
- [GymFrozenLakeQLearning.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/TPMLGymQLearning.ipynb) : solution au sujet de Q-Learning simple sur l'environnement "Lac Gelé" de Gym.
<!--- [GymFrozenLakeDoubleQLearning-Solution.ipynb](https://github.com/EmmanuelADAM/IntelligenceArtificiellePython/blob/master/GymFrozenLakeDoubleQLearning-Solution.ipynb) : solution au sujet de Double Q-Learning sur l'environnement "Lac Gelé" de Gym .-->
  - vérifier la performance du simple QLearning vs le Double QLearning
  - reprenez au choix le simple QLearning, ou le Double Q-Learning et appliquez l'algorithme sur l'environnement CliffWalking-v0 (point de départ en x, arrivée en T, coût de -1 par action sur o, -100 par action sur C).
   
```o  o  o  o  o  o  o  o  o  o  o  o```

```o  o  o  o  o  o  o  o  o  o  o  o```

```o  o  o  o  o  o  o  o  o  o  o  o```

```x  C  C  C  C  C  C  C  C  C  C  T```
